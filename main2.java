public class main2 {


	//basic gcd algorithm
	//note: returns gcd(a,0)=a
	static int gcd(int a, int b){
		if (b>0){
			return gcd(b,a%b);
		}
		else{
			return a;
		}
	}
	
	


	//returns true iff n is a prime power
	static boolean primePower(int n){
		int quotient =2;
		while (quotient <= n){
			if(n%quotient==0){
				while(n>1){
					if(n%quotient==0){
						n=n/quotient;
					}
					else{
						return false;
					}
				}
				return true;
			}
			quotient++;
		}
		System.out.println("primePower - this error should not be reached. n= " +n);
		return false;
	}

	//returns true iff n is a prime
	static boolean isPrime(int n){
		int quotient =2;
		while (quotient <= n){
			if(n%quotient==0){
				if(n==quotient){
					return true;
				}
				else{
					return false;
				}
			}
			quotient++;
		}
		System.out.println("isPrime - this error should not be reached. n= " +n);
		return false;
	}




	//Given a set (in the form of a set of booleans), 'k_base', this function returns the set 'k_base' multiplied 'power' times { sum_{i=1}^power a_i | a_i \in k_base }
	static boolean[] setPower(boolean[] k_base, int MAX_K, int power){
		boolean[] k_old = new boolean[MAX_K];
		boolean[] k_new = new boolean[MAX_K];


		for(int k=0; k<MAX_K; k++){
			k_old[k]=k_base[k];
			k_new[k]=k_base[k];
		}



		for(int n=2; n<=power; n++){ 
			//'k_old' is currently the set k_base multiplied n-1 times, i.e. { sum_{i=1}^{n-1} a_i | a_i \in k_base }  
			for(int i=0; i<MAX_K; i++){
				for(int j=0; j<MAX_K; j++){
					if(i+j<MAX_K){
						if(k_base[i] && k_old[j])
							k_new[i+j]=true;
					}
				}
			}
			//'k_new' is currently the set k_base multiplied n times, i.e. { sum_{i=1}^{n} a_i | a_i \in k_base }  
			for(int k=0; k<MAX_K; k++){
				k_old[k]=k_new[k];
			}
			//'k_old' is currently the set k_base multiplied n times, i.e. { sum_{i=1}^{n} a_i | a_i \in k_base } 
		}
		return k_new;
	}




	static int MAX_M =4100;
	static int PRINT_M =4099;
	static int MAX_K=MAX_M;
	static boolean[][] A = new boolean[MAX_M][MAX_K];















 




//Implements Theorem 2.9. 
	static void Marbach(){
		System.out.println("Generating From Marbach");
		for(int m=33; m<MAX_M; m+=2){
			int d1=(m-9)%18 /2;
			int d2=(m-11)%22 /2;
			for(int k=6; k<=m-Math.min(d1,d2+3); k++){
				if((m==51 && k==22) ||(m==53 && k==23)){
					//the two exceptional cases; dont do anything
				}
				else{
					A[m][k]=true;
				}
			}

		}
		
		System.out.println("Done");

		System.out.println("Finished Generating From Marbach");
	}






//	Implements Theorem 2.8
	static void BahgeriMain(){
		System.out.println("Generating From Bahgeri-BASE");
		//k=3
		for(int m=3; m<MAX_M; m+=3){
			A[m][3]=true;
		}
		//k=4
		A[4][4]=true;
		A[5][4]=true;
		A[8][4]=true;
		A[9][4]=true;
		A[10][4]=true;
		for(int m=12; m<MAX_M; m++){
			A[m][4]=true;
		}
		//k=5->13
		for(int k=5; k<=13; k++){
			for(int m=k; m<MAX_M; m++){
				A[m][k]=true;
			}
		}
		//k=15
		for(int m=15; m<MAX_M; m++){
			A[m][15]=true;
		}

		System.out.println("Finished Generating From Bahgeri-BASE");
	}


	//Implemented Thm 3.2, the generalisation of Cor 2.6.
	static void BahgeriThm3(){   //LargeSetIdemp

		System.out.println("Generating From (3,m-1,m)");
		for(int m=5; m<MAX_M; m++){
			A[m][m-1]=true;
		}
		System.out.println("Finished Generating From (3,m-1,m)");
	}

//	Construction 2.3 from our paper (originally thm7 in predecessor paper)
	static void additionConstruction(){
		for(int m1=3; m1<MAX_M; m1++){
			for(int m2=3; m2<MAX_M; m2++){
				for(int k=3; k<=Math.min(m1, m2); k++){
					if(A[m1][k]&&A[m2][k]){
						if(m1+m2<MAX_M){
							A[m1+m2][k]=true;
						}
					}
				}
			}
		}
	}



//	Implements Const2.4 between a (2,k_1,m_1)-Latin trade and a (2,k_2,m_2)-Latin trade (See theorem 2.1)- but we do not implement when m1 or m2 can be 2...see below
	static void BahgeriCor2(){ //multiplicationConstruction
		for(int m1=3; m1<MAX_M; m1++){
			for(int m2=m1; m2<MAX_M/m1+1; m2++){
				if( m1*m2<MAX_M){
					for(int k1=3; k1<=m1; k1++){
						for (int k2=3; k2<=m2; k2++){ 
							A[m1*m2][k1*k2]=true;
						}
					}
				}
			}
		}
	}


	//Implements the second part of Const2.4, when m2 = 2; but we dont take the case when m1=2, as we already know all (3,4,m)LT's exist.
	static void doublingConstruction(){ //BageriCor2-part2
		for(int m1=3; m1<MAX_M/2+1; m1++){
			for(int k1=3; k1<=m1; k1++){
				if(2*m1<MAX_M){ 
					A[2*m1][2*k1]=true;
				}
			}
		}
	}




	// Implements Construction 2.5
	static void BahgeriThm2Construction(int max_l, int max_m){
		for(int l=3; l<=max_l; l++){
			//System.out.println(l);
			if (l==6){
					//do nothing in this case
			}
			else{
				for(int m=5; m < max_m; m++){
					boolean[] k_base = A[m];
					boolean[] resulting;
					if(m*l<MAX_M){
						resulting=setPower(k_base,m*l,l);
						for(int k=0; k<m*l; k++){
							if(resulting[k]){
								A[m*l][k]=true;
							}
						}
					}
				}
			}
		}
	}





















//	Implements Theorem 3.13
	static void sHomoz(){

		System.out.println("Generating From s-Homoz");
		for(int lambda=6; lambda<Math.sqrt(MAX_M); lambda++){
			for(int b=1; b<lambda; b++){
				for(int a=b+1; a<(MAX_M-lambda*lambda)/lambda; a++){
					int m= lambda*(lambda+a)+b;
					if(gcd(m,lambda)==1){
						for(int k=15; k<=lambda*lambda; k++){
							A[m][k]=true;
						}
						int k=9;
						A[m][k]=true;
						k=12;
						A[m][k]=true;
					}
					else{
					}
				}
			}
		}
		int lambda=3;
		for(int b=1; b<lambda; b++){
			for(int a=b+1; a<(MAX_M-lambda*lambda)/lambda; a++){
				int m= lambda*(lambda+a)+b;
				if(gcd(m,lambda)==1){
					int k=9;
					A[m][k]=true;
				}
				else{
				}
			}
		}
		lambda=4;
		for(int b=1; b<lambda; b++){
			for(int a=b+1; a<(MAX_M-lambda*lambda)/lambda; a++){
				int m= lambda*(lambda+a)+b;
				if(gcd(m,lambda)==1){
					int k=9;
					A[m][k]=true;
					k=12;
					A[m][k]=true;
					k=15;
					A[m][k]=true;
					k=16;
					A[m][k]=true;
				}
				else{
				}
			}
		}
		lambda=5;
		for(int b=1; b<lambda; b++){
			for(int a=b+1; a<(MAX_M-lambda*lambda)/lambda; a++){
				int m= lambda*(lambda+a)+b;
				if(gcd(m,lambda)==1){
					for(int k=18; k<=25; k++){
						A[m][k]=true;
					}
					int k=9;
					A[m][k]=true;
					k=12;
					A[m][k]=true;
					k=15;
					A[m][k]=true;
					k=16;
					A[m][k]=true;
				}
				else{
				}
			}
		}
		
		System.out.println("Finished Generating From s-Homoz");
	}












//Implements Corollary 3.13, when d_{infinity} is 0 or 1
	static void ZhuNew(int max_n){
		System.out.println("Generating From Zhu's Construction");
		//over all alpha
		boolean[] D_SUM = new boolean[MAX_M];
		boolean[] D_SUM_TEMP = new boolean[MAX_M];
		boolean[] D = new boolean[MAX_M];
		
		
		for(int alpha =6; alpha<max_n; alpha++){
			System.out.println(alpha);
			//over all gamma, up to alpha-5
			for(int gamma=0; gamma<alpha - 5; gamma++){
				

				for(int i=0; i<MAX_M; i++){ 
					D[i]=false;
					D_SUM[i]=false;
					D_SUM_TEMP[i]=false;
				}
				boolean test;

				//If there exists a (3,m-d,m)-LT for each possible block size m,
				//	set D[d]=true. Otherwise set is to false.
				for(int d=1; d<=alpha-(gamma+1)-3; d++){ //this goes over the possible d, s.t. there exists a trade with k=|b|-d for each relevant b
					test=true;
					for(int i=0; i<=gamma+1; i++){
						if(alpha-(gamma+1)-d+i>=3){	//this goes over the possible lengths of blocks
							if (A[alpha-(gamma+1)+i][alpha-(gamma+1)-d+i]==false){
								test=false;
								break;
							}
						}
					}
					D[d]=test;
				}


				//D_SUM will be the the set D added n times, and adding either 
				//	0 or 1, for the R_{\infty} Resolution class
				for(int i=0; i<MAX_M-1; i++){ 
					if(D[i]){
						D_SUM[i] = true;
						D_SUM[i+1] = true;
					}
				}




				for(int n=2; n<= max_n; n++){

					for(int i=0; i<MAX_M; i++){
						D_SUM_TEMP[i]=false;
					}

					for(int i=0; i<MAX_M; i++){
						for(int j=0; j<MAX_M; j++){
							if(D_SUM[i] && D[j] && i+j<MAX_M){
								D_SUM_TEMP[i+j]=true; 
							}
						}
					}
					for(int i=0; i<MAX_M; i++){
						D_SUM[i] = D_SUM_TEMP[i];
					}
					if(primePower(n)&&n>=alpha){ 
						for(int u=1; u<=n-5; u++){
							int m=alpha*n-(n-5)*gamma-u;
							if(m==1213){
								System.out.println("n: " + n + "  alpha: " + alpha+ "  gamma: " + gamma + "  u: " + u);
							}
							if(m<MAX_M){
								for(int d=0; d<m; d++){
									if(D_SUM[d]&& m<MAX_M && m+n-d>=0){ 
										if(n-d>0){System.out.println("d= " + d + "  m= " + m + "  alpha= " + alpha + " gamma = " + gamma + "  n = " + n);}
										A[m][m+n-d]=true;  
									}
								}
							}
						}
					}
				}
			} 
		}

		System.out.println("Finished Generating From Zhu News Construction");
	}






	
//Implements Corollary 3.13, when gamma=0 and d_{infinity} can be >1
	static void ZhuNew_Gamma0(int max_n){
		System.out.println("Generating From Zhu's Construction _ Gamma=0");
		//over all alpha
		for(int alpha =6; alpha<max_n; alpha++){
			System.out.println(alpha);
			//over all gamma, up to alpha-5
			for(int gamma=0; gamma<1; gamma++){
				boolean[] D_SUM = new boolean[MAX_M];
				boolean[] D_SUM_TEMP = new boolean[MAX_M];
				boolean[] D = new boolean[MAX_M];
				boolean[] D_INFINITY = new boolean[MAX_M];

				for(int i=0; i<MAX_M; i++){
					D[i]=false;
					D_SUM[i]=false;
					D_SUM_TEMP[i]=false;
					D_INFINITY[i]=false;
				}
				boolean test;

				//If there exists a (3,m-d,m)-LT for each possible block size m,
				//	set D[d]=true. Otherwise set is to false.
				for(int d=1; d<=alpha-(gamma+1)-3; d++){ //this goes over the possible d, s.t. there exists a trade with k=|b|-d for each relevant b
					test=true;
					for(int i=0; i<=gamma+1; i++){
						if(alpha-(gamma+1)-d+i>=3){	//this goes over the possible lengths of blocks
							if (A[alpha-(gamma+1)+i][alpha-(gamma+1)-d+i]==false){
								test=false;
								break;
							}
						}
					}
					D[d]=test;
				}


				//D_SUM will be the the set D added n times, and adding either 
				//	0 or 1, for the R_{\infty} Resolution class
				for(int i=0; i<MAX_M-1; i++){ 
					if(D[i]){
						D_SUM[i] = true;
					}
				}



				for(int n=2; n<= max_n; n++){

					for(int i=0; i<MAX_M; i++){
						D_SUM_TEMP[i]=false;
					}
					
					

					for(int i=0; i<MAX_M; i++){
						for(int j=0; j<MAX_M; j++){
							if(D_SUM[i] && D[j] && i+j<MAX_M){
								D_SUM_TEMP[i+j]=true; 
							}
						}
					}
					for(int i=0; i<MAX_M; i++){
						D_SUM[i] = D_SUM_TEMP[i];
					}
					if(primePower(n)&&n>=alpha){ 
						for(int u=1; u<=n-5; u++){
							//resets D_INFINITY
							for(int i=0; i<MAX_M; i++){
								D_INFINITY[i]=false;
							}
							
							
							//Fills in D_INFINITY with possible values
							for(int d=0; d<=n-u; d++){
								if(A[n][n-d] && A[n-u][n-u-d]){
									D_INFINITY[d]=true;
								}
							}
							
							int m=alpha*n-(n-5)*gamma-u;
							if(m==1213){
								System.out.println("GAMMA0:  n: " + n + "  alpha: " + alpha+ "  gamma: " + gamma + "  u: " + u);
							}
							if(m<MAX_M){
								for(int nn=0; nn<n; nn++){
									for(int d=0; d<m; d++){
										if(D_SUM[d]&& D_INFINITY[nn] && m<MAX_M && m+n-d>=0){ 
											if(n-d>0){System.out.println("d= " + d + "  m= " + m + "  alpha= " + alpha + " gamma = " + gamma + "  n = " + n);}
											if(A[m][m+n-d-nn]==false){System.out.println("found one at m= " + m);}
											A[m][m+n-d-nn]=true;  
										}
									}
								}
							}
						}
					}
				}
			} 
		}

		System.out.println("Finished Generating From Zhu New Gamma0 Construction");
	}


	
	






//Loops over each m, and if the spectrum is not complete, prints the k such A[m][k] is false.
	static void Print(int min_to_print, int max_to_print){
		String out;
		boolean test;
		for(int m=min_to_print; m<=max_to_print; m++){
			test=false;
			for(int k=4; k<=m; k++){
				if(m<MAX_M){
					if(A[m][k]==false){test=true;}
				}
			}
			if(test){
				out=""+m+": ";
				for(int k=0; k<=m; k++){
					if(A[m][k]){}//out+=" "+1;}
					else{out+=" "+k;};
				}
				System.out.println(out);
			}
		}
	}




	public static void main(String[] args) {
		System.out.println("This program will compose the known existence properties of 3-way k-homogeneous Latin trades");





		for(int m=0; m<MAX_M; m++){
			for(int k=0; k<MAX_K; k++){
				A[m][k]=false;
			}
			A[m][0]=true;
			if(m>=3){
				A[m][m]=true;
			}
			if(m>=5){
				A[m][m-1]=true;
			}
		}




		Marbach(); 
		BahgeriMain();
		BahgeriThm3();


		

		System.out.println("doubling construction start");
		doublingConstruction();

		System.out.println("BahgeriCor2 start");
		BahgeriCor2();

		System.out.println("doubling construction start");
		doublingConstruction();


		sHomoz();
		
		System.out.println("addition construction start");
		additionConstruction();
		

		BahgeriThm2Construction(20,MAX_M);
		
		
		Print(0,200);
		ZhuNew(70); 
		ZhuNew_Gamma0(70);
		
		Print(0,PRINT_M);

	}
}